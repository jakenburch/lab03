<?php
function subtract($num1,$num2){
  $sum = $num1 - $num2;  // all lines must end in a semi-colon (;)
  return $sum;      // functions that do not have a return will return undefined
}

function colorCheck($num){
    if ($num > 0){ 
    echo "<span style='color:green;'>".$num."</span>"; 
    }else{
    echo "<span style='color:red;'>".$num."</span>"; 
    } 
}

$total = subtract($_POST["num1"],$_POST["num2"]);

?><!DOCTYPE html>
<html>
    <head>
    
    </head>
    <body>
        <h4>Numbers</h4>
        <?=colorCheck($_POST["num1"]);?>, <?=colorCheck($_POST["num2"]);?>
        <h4>Sum of numbers</h4>
        <?=colorCheck($total);?> <br/>
    
        <a href="index.html" title="home">return</a>
    </body>
</html>